package cn.yunhe.test;

import cn.yunhe.dao.IUserDao;
import cn.yunhe.entity.User;
import cn.yunhe.services.IUserServers;
import com.github.pagehelper.Page;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2018/5/14.
 */
public class Demo {

    @Test
    public void queryData() {
        ApplicationContext ac = new ClassPathXmlApplicationContext("spring/spring-mybatis.xml");
       IUserDao userDao = (IUserDao) ac.getBean("userDao");
        Map map = new HashMap<>();
        map.put("pageSize", 3);
        map.put("pageNum", 1);
        map.put("user", new User(""));

        int count = userDao.getLikeUsersCount(map);

        System.out.println("count===" + count);

        IUserServers userServers = (IUserServers) ac.getBean("userService");
      /*  Map map = new HashMap<>();
        map.put("pageSize", 3);
        map.put("pageNum", 1);
        map.put("user", new User(""));*/
//        Page page = userServers.queryListLikeUsers(map);



    }
}
