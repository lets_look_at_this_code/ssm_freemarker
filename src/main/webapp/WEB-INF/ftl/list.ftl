<!--Administrator on 2018/5/14.-->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/laypage.css">

    <script src="/js/vue/vue.min.js"></script>
    <script src="/js/jquery/jquery.min.js"></script>
    <script src="/js/laypage/laypage.js" charset="utf-8"></script>
    <script src="/js/layer/layer.js" charset="utf-8"></script>
</head>
<body>
<div id="app" class="container">
    <form class="form-inline bg-danger" role="form">
        查询条件：
        <div class="form-group">
            <label class="sr-only" for="username">用户名称</label>
            <input type="text" class="form-control" id="username" placeholder="用户名称">
        </div>
        <div class="form-group">
            <label class="sr-only" for="userage">用户年龄</label>
            <input type="text" class="form-control" id="userage" placeholder="用户年龄">
        </div>
        <button type="button" id="findUser" class="btn btn-success">查询用户</button>
        <button type="button" id="addUserBtn" class="btn btn-danger">增加用户</button>
    </form>

    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr class="success">
                <td>用户</td>
                <td>年龄</td>
                <td>操作</td>
            </tr>
            </thead>
            <tbody>
            <tr class="active" v-for="(item,index) in result">
                <td>{{item.name}}</td>
                <td>{{item.age}}</td>
                <td><a href="#" @click="editEvent(item.id)">编辑</a> <a href="#" @click="delEvent(item.id)">删除</a></td>
            </tr>
            </tbody>
            <tr>
                <td colspan="3">
                    <div id="pagenav"></div>
                </td>
            </tr>
        </table>
    </div>
</div>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            result: []
        }
    });

    var getUserPageList = function (curr) {
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: '/user/getPage.do',
            data: {
                pageNum: curr || 1,
                pageSize: 3,
                name: $("#username").val()
            },
            success: function (msg) {
                app.result = msg.page;
                laypage({
                    cont: 'pagenav',
                    pages: msg.totalPage,
                    first: '老一',
                    last: '老幺',
                    curr: curr || 1,
                    jump: function (obj, first) {
//                        console.log("obj====" + obj + ",first====" + first);
                        if (!first) {
                            getUserPageList(obj.curr);
                        }
                    }
                });
            }
        });
    }

    getUserPageList();


</script>

</body>
</html>