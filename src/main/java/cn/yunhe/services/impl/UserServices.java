package cn.yunhe.services.impl;

import cn.yunhe.dao.IUserDao;
import cn.yunhe.entity.User;
import cn.yunhe.services.IUserServers;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2018/5/14.
 */
@Service("userServers")
public class UserServices implements IUserServers {

    @Resource
    private IUserDao userDao;


    @Override
    public Page queryListLikeUsers(Map<String, Object> cond) {
        Page page = new Page();
        //得到总条数
        page.setTotal(userDao.getLikeUsersCount(cond));
        //每页大小
        int pageSize = Integer.parseInt(String.valueOf(cond.get("pageSize")));
        page.setPageSize(pageSize);
        //当前页码
        int curPageNum = Integer.parseInt(String.valueOf(cond.get("pageNum")));
        page.setPageNum(curPageNum);
        //动态计算总页数
//        page.setPageNum((int) (page.getTotal() / pageSize + (page.getTotal() % pageSize == 0 ? 0 : 1)));
        //将条件传递给pageHelper
        page = PageHelper.startPage(curPageNum, pageSize);
        List<User> list = userDao.getLikeUsers(cond);

        return page;
    }

    public IUserDao getUserDao() {
        return userDao;
    }

    public void setUserDao(IUserDao userDao) {
        this.userDao = userDao;
    }
}
