package cn.yunhe.services;

import com.github.pagehelper.Page;

import java.util.Map;

/**
 * Created by Administrator on 2018/5/14.
 */
public interface IUserServers {

    Page queryListLikeUsers(Map<String, Object> cond);
}
