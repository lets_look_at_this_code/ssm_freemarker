package cn.yunhe.controller;

import cn.yunhe.entity.User;
import cn.yunhe.services.IUserServers;
import com.github.pagehelper.Page;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zs on 2017/12/20.
 * /**
 * 1.如果一个controller里面大部分返回的都是页面的话，使用@Controller，这时返回的都是页面
 * 2.如果大部分返回的都是Json或字符串的话，就使用@RestController。如果它碰到Stirng，就返回String
 * int返回int（即基本数据类型还返回相应的数据类型），但是对象会返回json
 * 3.此时(使用@RestController)如果某方法仍要返回视图，则使用ModelAndView
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private IUserServers userServers;

    @RequestMapping("/getPage")
    public Map queryUsersByPage(int pageNum, int pageSize, User user) {
        Map map = new HashMap();
        map.put("pageSize", pageSize);
        map.put("pageNum", pageNum);
        map.put("user", user);
        Page page = userServers.queryListLikeUsers(map);
        map.put("page", page);
        map.put("totalPage", page.getPages());
        return map;
    }

    @RequestMapping("/toList")
    public ModelAndView toList() {
        return new ModelAndView("list");
    }


    @RequestMapping("/test")
    public List<User> test() {
        User user1 = new User();
        user1.setId(1);
        user1.setName("张三");

        User user2 = new User();
        user2.setId(2);
        user2.setName("李四");

        User user3 = new User();
        user3.setId(3);
        user3.setName("王五");

        List<User> list = new ArrayList<User>();
        list.add(user1);
        list.add(user2);
        list.add(user3);

        return list;
    }

    @RequestMapping("/toIndex")
    public ModelAndView toIndex() {
        return new ModelAndView("index");
    }


    public IUserServers getUserServers() {
        return userServers;
    }

    public void setUserServers(IUserServers userServers) {
        this.userServers = userServers;
    }
}
