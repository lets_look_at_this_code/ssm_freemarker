package cn.yunhe.controller;

import cn.yunhe.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2018/5/14.
 */
@Controller
@RequestMapping("/free")
@SessionAttributes(names = "user")
public class TestController {

    @RequestMapping("/index")
    public ModelAndView toIndex() {
        ModelAndView mv = new ModelAndView();
        User user1 = new User();
        user1.setName("张三");

        User user2 = new User();
        user2.setName("李四");

        User user3 = new User();
        user3.setName("王五");

        List<User> list = new ArrayList<User>();
        list.add(user1);
        list.add(user2);
        list.add(user3);

        mv.addObject("list", list);
        mv.addObject("user", user1);
        mv.setViewName("index");
        return mv;
    }
}


