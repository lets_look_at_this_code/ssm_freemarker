package cn.yunhe.dao;

import cn.yunhe.entity.User;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2018/5/14.
 */
@Repository("userDao")
public interface IUserDao {

    /**
     * 模糊查询
     *
     * @param cond
     * @return
     */
    List<User> getLikeUsers(Map<String, Object> cond);

    /**
     * 总条数
     * @param cond
     * @return
     */
    Integer getLikeUsersCount(Map<String, Object> cond);
}
